require('./config/config.js')

var express = require('express')
var bodyParser = require('body-parser')

var {Todo} = require('./models/todo.js')
var {User} = require('./models/user.js')
var {authenticate} = require('./middleware/authenticate')

var ObjectId = require('mongoose').Types.ObjectId
var _ = require('lodash')


const port = process.env.PORT

var app = express()

app.use(bodyParser.json())

app.post('/todos', authenticate, (req, res) => {
  console.log(req.body)
  var todo = new Todo({
    text: req.body.text,
    _creator: req.user._id
  })

  
  todo.save().then((doc) => {
    console.log('Saved todo')
    res.send(doc)
  }, (e) => {
    console.log('Error saving todo')
    res.status(400).send(e)
  })
})

app.get('/todos', authenticate, (req, res) => {
  Todo.find({
    _creator: req.user._id
  }).then((todos) => {
    res.send({
      todos
    })
  }, (e) => {
    res.status(400).send(e)
  })

})

app.get('/todos/:id', (req, res) => {
  let id = req.params.id

  if (!ObjectId.isValid(id)) {
    return res.status(400).send('Invalid id')
  }


  Todo.findById(id).then((todo) => {
    if (!todo) {
      return res.status(404).send('There are no todo with provided Id')
    }

    res.status(200).send({ todo: todo })
  }, (e) => {
    return res.status(400).send(e)
  })
})

app.post('/todos/:id', (req, res) => {
  var id = req.params.id
  var body = _.pick(req.body, ['text', 'completed'])
  console.log(id)
  if (!ObjectId.isValid(id)) {
    return res.status(400).send('Invalid id')
  }

  if (_.isBoolean(body.completed) && body.completed) {
    body.completedAt = new Date().getTime()
  } else {
    body.completed = false
    body.completedAt = null
  }

  Todo.findByIdAndUpdate(id, { $set: body }, { new: true }).then((todo) => {
    if (!todo) {
      return res.status(404).send('There are no todo with provided Id')
    }

    res.status(200).send({ todo: todo })
  }).catch((e) => {
    return res.status(400).send(e)
  })

})

app.delete('/todos/:id', (req, res) => {
  let id = req.params.id

  if (!ObjectId.isValid(id)) {
    return res.status(400).send('Invalid id')
  }

  Todo.findByIdAndRemove(id).then((todo) => {
    console.log(todo)
    if (!todo) {
      return res.status(404).send('There are no todo with provided Id')
    }

    res.status(200).send('Success removed')
  }, (e) => {
    return res.status(400).send(e)
  })

})


app.post('/users', (req, res) => {
  var body = _.pick(req.body, ['email', 'password'])
  console.log('Users')

  console.log(body)
  var user = new User({
    email: body.email,
    password: body.password
  })

  user.save().then((user) => {
    console.log('Saved')
    return user.generateAuthToken()
  }).then((token) => {
    return res.header('x-auth', token).send(user)
  }).catch((e) => {
    console.log(e)
    res.status(400).send(e)
  })
})

app.get('/users/me', authenticate, (req, res) => {
  console.log('Users ME url')
  console.log(req.user)
  res.send(req.user)
})

app.post('/users/login', (req, res) => {
  var body = _.pick(req.body, ['email', 'password'])

  User.findByCredentials(body.email, body.password).then((user) => {
    // res.send(user)
    console.log('Send answer')
    return user.generateAuthToken().then((token) => {
      return res.header('x-auth', token).send(user)
    })
  }).catch((e) => {
    res.status(400).send()
  })
})

app.delete('/users/me/token', authenticate, (req, res) => {
  // console.log(req.token)
  console.log('DELETE')
  console.log(req.user)
  req.user.removeToken(req.token).then(() => {
    res.status(200).send()
  }, () => {
    res.status(400).send()
  })
})

app.listen(port, () => {
  console.log(`Server started on port ${port}...`)
})

module.exports = {app}