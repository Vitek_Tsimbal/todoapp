const expect = require('expect')
const request = require('supertest')
const {ObjectId} = require('mongodb')
const {app} = require('./../server.js')
const {Todo} = require('./../models/todo.js')
const { todos, populateTodos, users, populateUsers} = require('./seed/seed.js')
const {User} = require('./../models/user.js')

beforeEach(populateTodos)
beforeEach(populateUsers)

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    var text = 'Test todo text'

    request(app)
      .post('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .send({text})
      .expect(200)
      .expect((res) => {
        expect(res.body.text).toBe(text)
      })
      .end((err, res) => {
        if (err) {
          return done(err)
        }

        Todo.find().then((todos) => {
          expect(todos.length).toBe(3)
          expect(todos[2].text).toBe(text)
          done()
        }).catch((e) => done(e))
      })
  })

  it('should not create todo with invalid body data', (done) => {
    var text = ''

    request(app)
    .post('/todos')
    .set('x-auth', users[0].tokens[0].token)
    .send()
    .expect(400)
    .end((err, res) => {
      if (err) {
        return done(err)
      }

      Todo.find().then((todos) => {
        expect(todos.length).toBe(2)
        done()
      }).catch((e) => done(e))
    })
  })
})

describe('GET /todos', () => {
  it('should get all todos', (done) => {
    request(app)
      .get('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.todos.length).toBe(1)
      }).end(done)
  })
})


describe('GET /todos/:id', () => {
  it('should return todo', (done) => {
    request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(todos[0].text)
      }).end(done)
  })

  it('should return 404 if todo not found', (done) => {
    var id = new ObjectId().toHexString()

    request(app)
      .get(`/todos/${id}`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(404)
      .end(done)
  })

  it('should return error if todo id is invalid', (done) => {
    request(app)
      .get(`/todos/123`)
      .set('x-auth', users[0].tokens[0].token)
      .expect(400)
      .end(done)
  })
})

describe('DELETE /todos/:id', () => {
  it('should remove a todo', (done) => {
    var hexId = todos[1]._id.toHexString()
    request(app)
      .delete(`/todos/${hexId}`)
      .expect(200)
      .end(done)
  })

  it('should return 404 if todo not found', (done) => {
    var id = new ObjectId().toHexString()

    request(app)
      .delete(`/todos/${id}`)
      .expect(404)
      .end(done)
  })

  it('should return error if todo id is invalid', (done) => {
    request(app)
      .delete(`/todos/123`)
      .expect(400)
      .end(done)
  })
})

describe('PATCH /todos/:id', () => {
  it('should update a todo', (done) => {
    var hexId = todos[1]._id.toHexString()
    var text = 'It should be a new text'
    request(app)
      .post(`/todos/${hexId}`)
      .send({
        completed: true,
        text
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.todo.text).toBe(text)
        expect(res.body.todo.completed).toBe(true)
      })
      .end(done)
  })

  it('should clear completedAt when todo is not completed', (done) => {
    var id = new ObjectId().toHexString()

    request(app)
      .post(`/todos/${id}`)
      .expect(404)
      .end(done)
  })

  it('should return error if todo id is invalid', (done) => {
    request(app)
      .post(`/todos/123`)
      .expect(400)
      .end(done)
  })
})

describe('GET /users/me', () => {
  it('should return users if authenticated', (done) => {
    request(app)
      .get('/users/me')
      .set('x-auth', users[0].tokens[0].token)
      .expect(200)
      .expect((res) => {
        expect(res.body._id).toBe(users[0]._id.toHexString())
        expect(res.body.email).toBe(users[0].email)
      })
      .end(done)
  })
  it('should return 401 if user is not authenticated', (done) => {
    request(app)
    .get('/users/me')
    .expect(401)
    .expect((res) => {
      expect(res.body).toEqual({})
    })
    .end(done)
  })
})

describe('POST /users', () => {
  it('should create new user', (done) => {
    var email = 'example@test.com'
    var password = '123aaa'
    request(app)
      .post('/users')
      .send({ email, password})
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist()
        expect(res.body._id).toExist()
        expect(res.body.email).toBe(email)
      })
      .end((err) => {
        if (err) {
          return done(err)
        }

        User.findOne({email}).then((user) => {
          expect(user).toExist()
          expect(user.password).toNotBe(password)
          done()
        }).catch(e => done(e))
      })
  })
  it('should not create user if email in use', (done) => {
    var email = 'example'
    var password = '123aaa'
    request(app)
      .post('/users')
      .send({ email, password})
      .expect(400)
      .end(done)
  })

  it('should not create user', (done) => {
    request(app)
      .post('/users')
      .send({
        email: users[0].email,
        password: '123aaa'
      })
      .expect(400)
      .end(done)
  })
})

describe('POST /user/login', () => {
  it('should login user and return token', (done) => {
    request(app)
      .post('/users/login')
      .send({
        email: users[0].email,
        password: users[0].password
      })
      .expect(200)
      .expect((res) => {
        expect(res.headers['x-auth']).toExist()

      })
      .end((err, res) => {
        if (err) {
          return err
        }

        User.findById(users[0]._id).then((user) => {
          expect(user.tokens[1].token).toBe(res.headers['x-auth'])
          done()
        }).catch(e => done(e))
      })
  })

  it('should reject invalid token', (done) => {
    request(app)
    .post('/users/login')
    .send({
      email: users[0].email + '123',
      password: users[0].password + '1'
    })
    .expect(400)
    .expect((res) => {
      expect(res.headers['x-auth']).toNotExist()
    })
    .end(done)
  })
})
