const {MongoClient, ObjectID} = require('mongodb')

var obj = new ObjectID()
console.log(obj)

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
  if (err) {
    return console.log(`Error to connection to Mongo: ${err}`)
  }
  const db = client.db('TodoApp')
  console.log('Connected to MongoDB database')

  db.collection('Todos').find().toArray().then((docs) => {
    console.log('Todos: ')
    console.log(JSON.stringify(docs, undefined, 2))
  }, (err) => {
    console.log('Unable to fetch todos: ' + err)
  })


  client.close()
})